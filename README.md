# Video Ideas

Ideas of videos to be published at [Reproducible Research Scout's YouTube channel](https://www.youtube.com/channel/UCHWHhQGAHx9dxRXHPMsYMiw/).

## Suggesting Videos

Create an issue at https://gitlab.com/reproducible-research-scout/video-ideas/-/issues
with you suggestion
or
email us at incoming+reproducible-research-scout-video-ideas-34771220-issue-@incoming.gitlab.com.

## Voting on Suggestions

Use the "thumbs up" feature (blue arrow in screenshot) to vote in videos that you want to see earlier.

![Thumbs up](img/thumbs-up-with-arrow.png)